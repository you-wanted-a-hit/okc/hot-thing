import json

class ChartEntry:
    def __init__(self, entry, date=None):
        self.title          = entry['title']
        self.artist         = entry['artist']
        self.is_new         = entry['isNew']
        self.last_pos       = entry['lastPos']
        self.peak_pos       = entry['peakPos']
        self.rank           = entry['rank']
        self.weeks          = entry['weeks']
        # self.snapshot_date  = date

    def __repr__(self):
        return str(self.serialize())

    def serialize(self):
        return json.loads(json.dumps(self, default=lambda o: o.__dict__))
