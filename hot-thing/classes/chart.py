from classes.chart_entry import ChartEntry
import json

class Chart:
    def __init__(self, info):
        self.title      = info['title']           
        self.name       = info['name']
        self.date       = info['date']
        self.next_date  = info['nextDate']
        self.prev_date  = info['previousDate']

        self.entries = [ChartEntry(entry, self.date) for entry in info['entries']]
    
    def __repr__(self):
        return str(self.serialize())

    def serialize(self):
        return json.loads(json.dumps(self, default=lambda o: o.__dict__))
