import json
from kafka import KafkaProducer
from decouple import config

def serialize_value(msg):
    if msg['status'] == 200:
        msg['response'] = msg['response'].serialize()
    return msg

class Producer:
    def __init__(self, host, port):
        self.producer = KafkaProducer(
            bootstrap_servers   = [f'{host}:{port}'],
            value_serializer    = lambda val: json.dumps(serialize_value(val)).encode('utf-8'),
            key_serializer      = lambda key: key.encode('utf-8'))
            
        
    def publish(self, key, value, topic):
        self.producer.send(topic, key=key, value=value)
    
    def close(self):
        self.producer.close()