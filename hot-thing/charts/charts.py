# Built-in modules
import json
from datetime import datetime

# External Dependencies
import billboard

# Classes
from classes.chart import Chart

# logging
from log import log
logger = log.get_logger('CHARTS')

def create_error_msg(chart_json, level, date_iso) -> str:
    msg = None
    
    if level not in billboard.charts():
        msg = f'Chart {level} is not a supported chart name'
    elif datetime.strptime(date_iso, "%Y-%m-%d") > datetime.today():
        msg = f'Date {date_iso} is in the future'
    elif not chart_json:
        msg = f'No chart for {level}, {date_iso}'
    elif not chart_json['entries']:
        msg = f'No available entries for {level}, {date_iso}'
    else:
        msg = f'Unknown error for {level}, {date_iso} resulted in {chart_json}'
    
    return msg

def create_response(level, date_iso,chart_json, ex):
    # response format - regardless of error or OK
    # date_iso    = date if date else datetime.today().isoformat()
    
    resp        = {
        'status'    : 0,
        'request'   : {
            'level' :   level,
            'date'  :   date_iso
        }
    }
    err_msg     = create_error_msg(chart_json,level,date_iso)

    if chart_json:
        if chart_json['entries']:
            resp['status'] = 200
            resp['response']    = Chart(chart_json)
        else:
            resp['status'] = 404
            resp['error']       = err_msg
    elif ex:
        resp['status'] = 400
        resp['exception']   = ex
        resp['error']       = err_msg

    
    logger.info(f'{resp["status"]} for {chart_json}, {level}, {date_iso}')

    return resp

def get_chart(level, date=None):
    resp    = None
    chart   = None
    
    date_iso = date if date else datetime.today().strftime('%Y-%m-%d')
    logger.info(f'Extracting {level} chart from {date_iso}')
    try:
        chart = json.loads(billboard.ChartData(level, date=date).json())
        resp = create_response(level=level, date_iso=date_iso, chart_json=chart, ex=None)
    except Exception as ex:
        logger.exception(f"Exception happened when extracting {date_iso}\'s {level}")
        resp = create_response(level=level, date_iso=date_iso,chart_json=chart, ex=ex)
    logger.info(f'Successfully extracted {level} chart from {date_iso} ')
    logger.debug(resp)

    return resp