# import schedule
from decouple import config

import threading
import queue

from charts import charts
from consumer.consumer import Consumer
from producer.producer import Producer
from utils import utils

# Configuring logging
from log import log


task_queue = queue.Queue()

# def publish_new_chart():
#     kafka_host      = config('KAFKA_HOST_NAME')
#     kafka_port      = config('KAFKA_HOST_PORT')
#     out_topic       = config('KAFKA_PRODUCER_TOPIC')
#     kproducer       = Producer(host=kafka_host, port=kafka_port)

#     chart       = charts.get_chart('hot-100')
#     msg_key     = 'update'
#     msg_value   = chart.json()
    
#     kproducer.publish(key=msg_key, value=msg_value, topic=out_topic)
#     kproducer.close()

def consume_daemon():
    logger      = log.get_logger("C-DAEMON")
    kafka_host  = config('KAFKA_HOST_NAME')
    kafka_port  = config('KAFKA_HOST_PORT')
    in_topic    = config('KAFKA_CONSUMER_TOPIC')

    logger.info('Finished setting up')

    consumer  = Consumer(host=kafka_host, port=kafka_port, topics=in_topic)
    for message in consumer.consumer:
        logger.info(f'New message received {message.key} -> {message.value}')
        task_queue.put(message)
        logger.info(f'New message is in queue, waiting to be proccessed')

def run():
    # Extracts new chart on weekly basis
    # schedule.every().friday.at('04:30:00').do(publish_new_chart)

    # Setting up logger
    logger  = log.get_logger('PROD LOOP')
    
    # Configuring Kafka Producer
    kafka_host      = config('KAFKA_HOST_NAME')
    kafka_port      = config('KAFKA_HOST_PORT')
    producer_topic  = config('KAFKA_PRODUCER_TOPIC')   
    producer        = Producer(host=kafka_host, port=kafka_port)

    logger.info('Finished setting up')
    # Starting Consumer daemon
    threading.Thread(target=consume_daemon, daemon=True).start()

    while True:
        message = task_queue.get()
        if utils.validate_message(message):
            logger.info('Message is in valid format')
            logger.info('Requesting chart')
            name    = message.value['name']
            date    = message.value['date'] if 'date' in message.value else None
            chart   = charts.get_chart(level=name, date=date)
            producer.publish(message.key, chart, producer_topic)
        else:
            logger.info('Message is not in a valid format')
            err_json = {
                'status': 400,
                'request'   : message.value,
                'error_msg' : 'The sent message is not in a valid format'
            }
            producer.publish("error", err_json, producer_topic)
