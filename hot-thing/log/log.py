import logging

def get_logger(name, level=logging.DEBUG):
    formatter = logging.Formatter(fmt='[ %(asctime)s ] %(levelname)-4s %(name)-4s {%(module)s:%(funcName)s:%(lineno)s} %(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger